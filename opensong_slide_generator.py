import xml.etree.ElementTree as ET
from datetime import date, timedelta
import os.path
from tkinter import *
from tkinter import ttk

# VARIABLES
templateName = 'auto_template'  # template name
song_folder = '/Users/Sean/Dropbox/FPCOC/OpenSong/Songs/'
set_folder = '/Users/Sean/Dropbox/FPCOC/OpenSong/Sets/'

sermon = {'sermon_title':'',
          'scripture_ref':''}
global scripture_ref  #'Book Ch:v-v'
scripture_ref = ''
global sermon_title  #'Sermon Title'
sermon_title = ''
# list to store songs, each sub list [songName, custom presentation order, existAlready]
num_of_worship_songs = 3
# worship_songs = [
#    ['Cornerstone',''],
#    ['Man of Sorrows','V1 V2 C']
#]
worship_songs = []
num_of_response_songs = 1
#response_songs = [
#    ['Mighty to Save', '']
#]
response_songs = []
# Functions
# Takes a list of song names, and checks them against config song_folder, then appends result at end of list
def song_from_list_exists(song_list = []):
    for song in song_list:
        if os.path.isfile(song_folder + song[0]):
            song.append(1)  # status of existence, used to generate song XML later
        else:
            song.append(0)


# Takes list of songs, list of slides, and str placeholder name
# checks to see if songs exist, and if they do insert the songs into where the placeholder is
def insert_songs_to_placeholder(song_list, slide_list, placeholder_name):
    song_from_list_exists(song_list)
    # find all worship song place holders
    xpath = 'slide_group/[@name=\"' + placeholder_name +'\"]'
    selected_slides = slide_list.findall(xpath)

    for i, slide in enumerate(selected_slides):
        if i < len(song_list) and song_list[i][2] == 1:
            slide.set('name', song_list[i][0])
            slide.set('presentation', song_list[i][1])
            print(slide.attrib)
        else:
            slide_list.remove(slide)


# Takes all input items from the form and parse into variables
def parse_form():
    sermon['sermon_title'] = sermon_title_form.get()
    sermon['scripture_ref'] = scripture_ref_form.get()

    for entry in worship_entries:
        worship_songs.append([entry[0].get(),entry[1].get()])

    for entry in response_entries:
        response_songs.append([entry[0].get(),entry[1].get()])
def main():
    parse_form()


    # XML Parsing
    tree = ET.parse(set_folder+templateName)
    root = tree.getroot()

    # date to set defaults to nearest sunday in format MM/DD/YY
    today = date.today()
    sunday = today + timedelta((6 - today.weekday()) % 7)

    # change set name to next sunday mmddyyyy
    set_name = root.find('[@name]')
    set_name.set('name', sunday.strftime("%m%d%Y"))

    # set slide_groups as new root for all slides
    slide_groups = root[0]

    # find and modify Date on 'Welcome Slide'
    worship_date = slide_groups.find('slide_group/[@name="Welcome Slide"]/subtitle')
    worship_date.text = 'Worship ' + sunday.strftime("%m/%d/%y")

    # Auto Adding worship songs if they exist in song folder
    insert_songs_to_placeholder(worship_songs, slide_groups, 'zzworship_song_place_holder')

    # Update Sermon slide with sermon title and scripture Reference
    sermon_slide_title = slide_groups.find('slide_group/[@name="Sermon"]/title')
    sermon_slide_title.text = sermon['sermon_title']
    print(sermon_slide_title.text)
    sermon_slide_subtitle = slide_groups.find('slide_group/[@name="Sermon"]/subtitle')
    sermon_slide_subtitle.text = sermon['scripture_ref']

    # Auto Adding response songs if they exist in song folder
    insert_songs_to_placeholder(response_songs, slide_groups, 'zzresponse_song_place_holder')

    # Save to XML file
    output_name = set_folder+sunday.strftime("%m%d%Y")
    tree.write(output_name)


root = Tk()
root.title("Slide Generator")
mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)


sermon_title_form = StringVar()
scripture_ref_form = StringVar()
ttk.Label(mainframe, text="Sermon Title").grid(column=0, row=0, sticky=E)
sermon_entry = ttk.Entry(mainframe, width = 16, textvariable = sermon_title_form)
sermon_entry.grid(column=1, row= 0, sticky=(W, E), columnspan = 2)
ttk.Label(mainframe, text="Scripture Reference").grid(column=0, row=1, sticky=E)
scripture_entry = ttk.Entry(mainframe, width = 16, textvariable = scripture_ref_form)
scripture_entry.grid(column=1, row= 1, sticky=(W, E), columnspan = 2)

worship_entries = []
response_entries = []

for i in range(num_of_worship_songs):
    worship_entries.append([StringVar(),StringVar()])

for i in range(num_of_response_songs):
    response_entries.append([StringVar(),StringVar()])

ttk.Label(mainframe, text="Worship Songs").grid(column=0, row=2)
ttk.Label(mainframe, text="Song Name").grid(column=1, row=2)
ttk.Label(mainframe, text="Presentation").grid(column=2, row=2)
for i, song in enumerate(worship_entries):
    song_entry = ttk.Entry(mainframe, width = 20, textvariable = song[0])
    song_entry.grid(column=1, row= 3+i, sticky=(W, E))
    presentation_entry = ttk.Entry(mainframe, width = 20, textvariable = song[1])
    presentation_entry.grid(column=2, row= 3+i, sticky=(W, E))

response_starting_row = 3+num_of_worship_songs+1
ttk.Label(mainframe, text="Response Songs").grid(column=0, row=response_starting_row-1)
ttk.Label(mainframe, text="Song Name").grid(column=1, row=response_starting_row-1)
ttk.Label(mainframe, text="Presentation").grid(column=2, row=response_starting_row-1)

for i, song in enumerate(response_entries):
    song_entry = ttk.Entry(mainframe, width = 20, textvariable = song[0])
    song_entry.grid(column=1, row= response_starting_row+i, sticky=(W, E))
    presentation_entry = ttk.Entry(mainframe, width = 20, textvariable = song[1])
    presentation_entry.grid(column=2, row= response_starting_row+i, sticky=(W, E))

ttk.Button(mainframe, text="Generate", command=main).grid(column= 1, row=5+num_of_worship_songs+num_of_response_songs, sticky=W, columnspan = 2)


for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

root.mainloop()