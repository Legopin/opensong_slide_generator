# README #

### What is this repository for? ###

* This is a small personal Python project to automate the creation of routine presentation slides using the OpenSong software. The goal is to utilize a small GUI to pre-populate regularly changed content, then manually inspect and correct for presentation needs.

### How do I get set up? ###

* Currently the script is still in development, so all values will be initially hard coded. Run the script and find out.

### Credit ###

* The file format for OpenSong [http://www.opensong.org/home/file-formats](Link URL)
* Link to the OpenSong [Project Website](http://www.opensong.org)